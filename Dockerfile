# ktlint helpers 2020-10-22 by @k33g | on gitlab.com 
FROM alpine:latest

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

RUN apk --update add --no-cache bash curl util-linux \
    nodejs npm \
    openjdk11
RUN curl -sSLO https://github.com/pinterest/ktlint/releases/download/0.39.0/ktlint
RUN chmod a+x ktlint
RUN mv ktlint /usr/local/bin/

COPY analyze.js /usr/local/bin/analyze

RUN chmod +x /usr/local/bin/analyze

CMD ["/bin/sh"]
