# Code Quality LiKtnt for GitLab CI

## Example

```yaml
stages:
  - 🔎code-quality
  
include:
  - project: 'tanuki-workshops/kube-demos/code-quality-analyzers/ktlint'
    file: 'ktlint.gitlab-ci.yml'

🔎:code:quality:ktlint:
  stage: 🔎code-quality
  extends: .ktlint:analyzer
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID
  script:
    - analyze src/**/*.kt
```
