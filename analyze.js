#!/usr/bin/env node

const fs = require("fs")
const { exec } = require("child_process")

let cmdArgs = process.argv.slice(2);
let directory = cmdArgs[0] || "src"
let reportName = cmdArgs[1] || "./ktlint-report.json" 

let cmd = `ktlint ${directory} --reporter=json,output=${reportName}`

exec(cmd, (error, stdout, stderr) => {

  if (error) {
    console.log(`😡 error(s) found: ${error.message}`)

    try {
      let content = fs.readFileSync(reportName)
      
      /* --- helpers --- */
      let btoa = (string) => Buffer.from(string).toString("base64")
      let fingerprint = (description, path, line, column) => `${btoa(description)}-${btoa(path)}-${btoa(line)}-${btoa(column)}`
    
      let ktlintResults = []
      
      JSON.parse(content).forEach(item => {
        // path
        let file = item.file //TODO: replace
        item.errors.forEach(error => {
          ktlintResults.push({
            filePath: file, 
            line: error.line,
            column: error.column,
            message: error.message,
            rule: error.rule
          })
        })
      })
      console.log("📝", ktlintResults)
      if(ktlintResults.length==0) {
        /* --- generate empty gl-code-quality-report.json --- */
        fs.writeFileSync("./gl-code-quality-report.json", "[]")
      } else {
        /* --- generate gl-code-quality-report.json --- */
        let codeQualityData = ktlintResults.map(item => {
          let fp = fingerprint(item.message, item.filePath, item.line.toString(), item.column.toString())
          return {
            description: `📝 ${item.rule} (${item.line}:${item.column}): ${item.message}`,
            fingerprint: fp,
            location: {
              path: item.filePath.replace(process.env.CI_PROJECT_DIR ,""),
              lines: {
                begin: item.line
              }
            }
          }
        })
    
        fs.writeFileSync("./gl-code-quality-report.json", JSON.stringify(codeQualityData, null, 2))

        return
      }
    } catch(error) {
      /* --- generate empty gl-code-quality-report.json --- */
      fs.writeFileSync("./gl-code-quality-report.json", "[]")
    }


  } else { 
    /* --- generate empty gl-code-quality-report.json --- */
    fs.writeFileSync("./gl-code-quality-report.json", "[]")
  }

  if (stderr) {
    console.log(`stderr: ${stderr}`)
    return
  }
  console.log(`stdout: ${stdout}`)
})

